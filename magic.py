import os
import threading
import queue
import time

import yolo3

PROCESS_QUEUE = queue.Queue()
RUNNER = None #current thread for running the queue

# util
def check_is_processed(project_name):
    return not os.path.isfile(os.path.join('../videos', project_name, 'lock'))
    
# threading
def start_the_queue():
    PROCESSING = None #current yolo thread
    while True:
        if not PROCESS_QUEUE.empty():
            process(PROCESS_QUEUE.get())

def put_to_queue(project_name):
    PROCESS_QUEUE.put(project_name)

def run():
    RUNNER = threading.Thread(target=start_the_queue)
    RUNNER.start()

# YOLO3
def process(project_name):
    # DEBUG
    # print("Pretend to runing YOLO3 on {}".format(project_name))
    # time.sleep(5)
    # print("Stop pretending to runing YOLO3")

    # Run YOLO3
    yolo3.YOLO('../videos/'+project_name)
    os.remove(os.path.join('../videos', project_name, 'lock'))



if __name__=="__main__":
    pass