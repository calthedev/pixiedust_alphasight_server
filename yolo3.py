from ctypes import *
import math
import random
import os
import cv2
import numpy as np
import time
import darknet

from scipy import stats, integrate
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

SUPPORTED_FILES = ['.avi', '.mp4', '.mov']
FRAMERATE = 60.0 #placeholder framerate
detection_list = []
current_frame = 0
person_count = []



def plot_heatmap():
    ax = sns.kdeplot([x[2] for x in detection_list], [y[3] for y in detection_list], cmap="Blues", shade=True, shade_lowest=False)
    ax.axis("off")

    fig = ax.get_figure()
    fig.canvas.draw()

    w,h = fig.canvas.get_width_height()

    img = np.fromstring(fig.canvas.tostring_argb(), dtype=np.uint8, sep='')
    img = img.reshape(fig.canvas.get_width_height()[::-1] + (4,))

    # img is rgb, convert to opencv's default bgr
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)

    #cv2.imwrite("heatmap %s.jpg" % time.time(), img)

    return img



def convertBack(x, y, w, h):
    xmin = int(round(x - (w / 2)))
    xmax = int(round(x + (w / 2)))
    ymin = int(round(y - (h / 2)))
    ymax = int(round(y + (h / 2)))
    return xmin, ymin, xmax, ymax

def logDetection(frame_id, i, x, y):
    detection_list.append((frame_id, i, x, y))


def cvDrawBoxes(frame_id, detections, img):
    for index, detection in enumerate(detections):
        x, y, w, h = detection[2][0],\
            detection[2][1],\
            detection[2][2],\
            detection[2][3]

        logDetection(frame_id, index, x, y)
        cv2.circle(img, (int(x), int(y)), 5, (0, 255, 0), -1)
    person_count.append(detection_list[-1][1]+1)
    return img


def getPersonCount():
    f = int(FRAMERATE)
    if current_frame < f:
        return person_count[-1]
    else:
        s = 0
        for i in person_count[current_frame-f:]:
            s += i
        return int(s/f)

def path(p, args=[]):
    o = p
    for arg in args:
        o = os.path.join(o, arg)
    return os.path.abspath(o)

netMain = None
metaMain = None
altNames = None

def get_original_video(project_path):
    files = os.listdir(project_path)
    for file in files:
        for ext in SUPPORTED_FILES:
            if ext in file:
                return path(project_path, [file])
    return None

def YOLO(project_path):
    
    s_time = time.time()

    global metaMain, netMain, altNames
    configPath = "./yolov3/yolov3-person.cfg"
    weightPath = "./yolov3/person-yolov3-spp_10000.weights"
    metaPath = "./yolov3/coco-person.data"
    if not os.path.exists(configPath):
        raise ValueError("Invalid config path `" +
                         os.path.abspath(configPath)+"`")
    if not os.path.exists(weightPath):
        raise ValueError("Invalid weight path `" +
                         os.path.abspath(weightPath)+"`")
    if not os.path.exists(metaPath):
        raise ValueError("Invalid data file path `" +
                         os.path.abspath(metaPath)+"`")
    if netMain is None:
        netMain = darknet.load_net_custom(configPath.encode(
            "ascii"), weightPath.encode("ascii"), 0, 1)  # batch size = 1
    if metaMain is None:
        metaMain = darknet.load_meta(metaPath.encode("ascii"))
    if altNames is None:
        try:
            with open(metaPath) as metaFH:
                metaContents = metaFH.read()
                import re
                match = re.search("names *= *(.*)$", metaContents,
                                  re.IGNORECASE | re.MULTILINE)
                if match:
                    result = match.group(1)
                else:
                    result = None
                try:
                    if os.path.exists(result):
                        with open(result) as namesFH:
                            namesList = namesFH.read().strip().split("\n")
                            altNames = [x.strip() for x in namesList]
                except TypeError:
                    pass
        except Exception:
            pass

    video = get_original_video(project_path)
    if not video:
        print("no video found")
        return
    cap = cv2.VideoCapture(video)
    cap.set(3, 1280)
    cap.set(4, 720)

    FRAMERATE = cap.get(cv2.CAP_PROP_FPS)

    fourcc = cv2.VideoWriter_fourcc(*'VP80')
    out = cv2.VideoWriter(
        path(project_path, ['out', "output.webm"]), fourcc, FRAMERATE,
        (1280, 720))
    hm_out = cv2.VideoWriter(
        path(project_path,['out',  "heatmap.webm"]), fourcc, FRAMERATE,
        (1280, 720))

    print("Starting the YOLO loop...")
    print(darknet.network_width(netMain))
    print(darknet.network_width(netMain))

    # Create an image we reuse for each detect
    darknet_image = darknet.make_image(darknet.network_width(netMain),
                                    darknet.network_height(netMain),3)
    while True:
        prev_time = time.time()
        ret, frame_read = cap.read()
        if not ret:
            break
        frame_rgb = cv2.cvtColor(frame_read, cv2.COLOR_BGR2RGB)
        frame_resized = cv2.resize(frame_rgb,
                                   (darknet.network_width(netMain),
                                    darknet.network_height(netMain)),
                                   interpolation=cv2.INTER_LINEAR)

        darknet.copy_image_from_bytes(darknet_image,frame_resized.tobytes())

        detections = darknet.detect_image(netMain, metaMain, darknet_image, thresh=0.25)
        current_frame = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
        image = cvDrawBoxes(current_frame, detections, frame_resized)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        print(1/(time.time()-prev_time))


        res = cv2.resize(image, (1280,720))

        font = cv2.FONT_HERSHEY_SIMPLEX
        topLeftCorner = (64,64)
        fontScale = 2
        fontColor = (237,41,57)
        lineType = 2
        count = str(getPersonCount()) 
        cv2.putText(res,count , topLeftCorner, font, fontScale,fontColor, lineType)

        out.write(res)
        hm = plot_heatmap()
        #hm_out.write(cv2.resize(hm, (1280,720)))
        hm_out.write(cv2.resize(hm,(1280, 720)))
        #save frame
        #cv2.imwrite("person %s.jpg" % prev_time, image)



        #cv2.imshow('Demo', image)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        cv2.waitKey(3)
            
    cap.release()
    out.release()
    hm_out.release()
    cv2.destroyAllWindows()
    video_length = detection_list[-1][0] // FRAMERATE
    avg_count = 0
    for i in person_count:
        avg_count += i
    avg_count //= detection_list[-1][0]
    processing_time = time.time() - s_time
    print("###RESULT###\n")
    print("YOLO cfg resolution: {}\n".format(darknet.network_width(netMain)))
    #print("YOLO detection threshold: {}\n".format(args.thresh))
    print("Length of video: {}\n".format(video_length))
    print("Average person count: {}\n".format(avg_count))
    print("Processing time: {}s\n".format(processing_time))
    with open(path(project_path, ['out', 'coordinates.txt']), 'a') as f:
        for frame_id, person_number, x, y in detection_list:
            f.write("{},{},{},{}\n".format(int(frame_id), person_number , int(x), int(y)))

if __name__ == "__main__":
    print("sorry")