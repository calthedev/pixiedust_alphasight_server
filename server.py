from flask import Flask, render_template, send_from_directory, request, redirect, url_for, flash
from werkzeug.utils import secure_filename

import os

from subprocess import call

import magic

app = Flask(__name__)
magic.run()

ALLOWED_EXTENSIONS = set(['mp4','avi'])

@app.route("/")
def index(videos=None):
    return render_template('index.html', projects=os.listdir('../videos'))

@app.route("/project/<project_name>")
def project(project_name=None, processed=False):
    if not 'lock' in os.listdir(os.path.join('../videos', project_name)):
        processed = True
    return render_template('project.html', project=project_name, processed=processed)

@app.route('/project/<project_name>/videos/<path:filename>')
def serve_video(filename, project_name):
    dir = os.path.join(os.getcwd(), '../videos', project_name, 'out')
    return send_from_directory(os.path.realpath(dir), filename)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/project/create', methods=['POST'])
def upload_file():
    # check if the post request has the file part
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    # if user does not select file, browser also
    # submit a empty part without filename
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        #[TODO] secure project name
        path = os.path.join(os.getcwd(),'../videos', request.values['project_name'])
        os.mkdir(path)
        os.mkdir(os.path.join(path,'out'))
        call(["touch", os.path.abspath(os.path.join('../videos',request.values['project_name'] , 'lock'))])
        file.save(os.path.join(path , filename))
        magic.put_to_queue(request.values['project_name'])
        return redirect(url_for('project', project_name=request.values['project_name']))